import {
    key
} from './../../../config';

export default class Recipe {
    constructor(id) {
        this.id = id
    }
    async getRecipe() {
        try {
            let resultCall = await fetch(`https://www.food2fork.com/api/get?key=${key}&rId=${this.id}`).then(response => response.json())
            console.log('resultCall: ', resultCall);
            this.title = resultCall.recipe.title
            this.author = resultCall.recipe.publisher
            this.img = resultCall.recipe.image_url
            this.url = resultCall.recipe.source_url
            this.ingredients = resultCall.recipe.ingredients
        } catch (error) {
            alert('problem api fetch recipe')
            console.log(error)
        }
    }
    calcTime() {
        this.time = Math.ceil(this.ingredients.length / 3) * 15
    }
    calcServing() {
        
        this.serving = 4
    }
    parseIngredients() {
        // const longUnit = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds']
        // const shortUnit = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound']
         const unit = {
             'tablespoons': 'tbsp',
             'tablespoon': 'tbsp',
             'ounces': 'oz',
             'ounce': 'oz',
             'teaspoons': 'tsp',
             'teaspoon': 'tsp',
             'cups': 'cup',
             'pounds': 'pound'
         }
        const newIngredients = this.ingredients.map(x => {
            let x = x.replace()
        })
        this.ingredients = newIngredients
    }

}