import { key } from './../../../config';

export default class Search {
    constructor(query) {
        this.query = query
    }

    //chimata api
    async getResult() {
            try {
            let resultCall = await fetch(`https://www.food2fork.com/api/search?key=${key}&q=${this.query}`).then(response => response.json())
            this.result = resultCall.recipes
            } catch (error) {
                alert('problem api fetching search')
            console.log(error)
        }
    }
}

