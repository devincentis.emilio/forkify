// Global app controller
import Search from './models/Search';
import {
    elements,
    renderLoader,
    clearLoader
} from './views/base';
import * as searchView from './views/searchView';
import Recipe from './models/Recipe';

//global state
const state = {}

const controlSearch = async () => {
    const query = searchView.getInput()
    if (query) {
        renderLoader(elements.searchResult)
        state.search = new Search(query)
        try {
            await state.search.getResult()
            console.log(state)
            searchView.renderResults(state.search.result)
            clearLoader()
        } catch (error) {
            console.log(error)
            alert('problem with search')
        }
    }
}
clearLoader()

elements.searchForm.addEventListener('submit', event => {
    event.preventDefault()
    controlSearch()
    searchView.clearResults()
    searchView.cleraField()
})
elements.searchResPages.addEventListener('click', event => {
    const btn = event.target.closest('.btn-inline')
    if (btn) {
        const goToPage = parseInt(btn.dataset.goto, 10)
        searchView.clearResults()
        searchView.renderResults(state.search.result, goToPage)
    }
})

const controlRecipe = async () => {
    const id = window.location.hash.replace('#', '')
    console.log('id: ', id);    
    if (id) {
        state.recipe = new Recipe(id)
        window.r = state.recipe
        try {
            await state.recipe.getRecipe()
            state.recipe.calcTime()
            state.recipe.calcServing()
        } catch (error) {
            console.log(error)
            alert('problem with recipe')
        }

    }

}
window.addEventListener('hashchange', controlRecipe)
if (window.location.hash) {
    window.addEventListener('load', controlRecipe)
}