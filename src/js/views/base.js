export const elements = {
    searchForm: document.querySelector('.search'),
    searchResult: document.querySelector('.results'),
    searchInput: document.querySelector('.search__field'),
    searchResList: document.querySelector('.results__list'),
    searchResPages: document.querySelector('.results__pages')

}

export const elementsStrings = {
    loader: 'loader'
}

export const renderLoader = parentDiv => {
    const loader = `<div class= "loader">
        <svg>
            <use href="img/icons.svg#icon-cw"></use>
        </svg>
    </div>`

    parentDiv.insertAdjacentHTML('afterbegin',loader)
} 
export const clearLoader = (x = document.querySelector(`.${elementsStrings.loader}`)) => x ? x.remove() : null

    

